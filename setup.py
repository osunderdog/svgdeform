from setuptools import setup

setup(name='SVGDrawArea',
      version='0.1',
      description='My SCG Drawing Classes.',
      url='http://www.wafermovement.com/SVGDrawing',
      author='Bruce W. Lowther',
      author_email='brucelowter@gmail.com',
      license='FOFOF',
      packages=['SVGDrawArea'],
      zip_safe=False,
      install_requires=['drawSvg'],
      python_requires='>3.0')
