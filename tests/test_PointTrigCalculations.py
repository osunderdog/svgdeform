import unittest
from math import pi, degrees, radians

from SVGDrawArea.Point import Point
from tests.BaseTestClass import BaseTestClass

TWOPI = 2 * pi  # type: float


class MyTestCase(BaseTestClass):
    def setUp(self) -> None:
        twopi = 2 * pi
        self.tblcase = [
            [(0, 1), (Point(x=1.0   , y=0.0   ))],
            [(45, 1), (Point(x=0.707 , y=0.707 ))],
            [(90, 1), (Point(x=0.0   , y=1.0   ))],
            [(135, 1), (Point(x=-0.707, y=0.707 ))],
            [(180, 1), (Point(x=-1.0  , y=0.0   ))],
            [(225 - 360, 1), (Point(x=-0.707, y=-0.707))],
            [(270 - 360, 1), (Point(x=0.0, y=-1.0))],
            [(315 - 360, 1), (Point(x=0.707, y=-0.707))],
            [(0, 1), (Point(x=1.0 , y=-0.0))]
        ]
    def test_fromAngleAbs_a(self):
        for i, cap in enumerate(self.tblcase):
            pt = Point.fromAngleAbs(angle=radians(cap[0][0]), radius=cap[0][1])
            self.assertAlmostEqual(pt.x, cap[1].x, 3, "x component case {} angle {} rad {}".format(i, degrees(cap[0][0]), cap[0][0]))
            self.assertAlmostEqual(pt.y, cap[1].y, 3, "y component caes {} angle {} rad {}".format(i, degrees(cap[0][0]), cap[0][0]))

    def test_angleDistTo(self):
        for i, cap in enumerate(self.tblcase):
            ad = Point(0,0).angleDistTo(cap[1])
            self.assertAlmostEqual(ad[0], radians(cap[0][0]), 3, "case: {}: expected angle {}; actual angle:{}".format(i, cap[0][0], degrees(ad[0])))
            self.assertAlmostEqual(ad[1], cap[0][1], 3, "case: {}: expected dist {}; dist rad:{}".format(i,  cap[0][1], ad[1]))


if __name__ == '__main__':
    unittest.main()
