import unittest
from SVGDrawArea.DrawArea import DrawArea
from tests.BaseTestClass import BaseTestClass

class TestStringMethods(BaseTestClass):

    def test_init(self):
        x = 10
        y = 100
        da = DrawArea(x,y)
        self.assertEqual('DrawArea: dx:10,dy:100',str(da))

if __name__ == '__main__':
    unittest.main()
