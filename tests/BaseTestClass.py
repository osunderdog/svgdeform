from unittest import TestCase


class BaseTestClass(TestCase):
    def __init__(self, *args, **kwargs):
        super(BaseTestClass, self).__init__(*args, **kwargs)

    def assertPoint(self, ap, ep, msg=None):
        self.assertAlmostEqual(ap.x, ep.x, 4, msg)
        self.assertAlmostEqual(ap.y, ep.y, 4, msg)

    def test_nothing(self):
        self.assertTrue(True)
