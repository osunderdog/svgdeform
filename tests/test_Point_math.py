import unittest
from math import pi, sqrt, atan2

from SVGDrawArea.Point import Point
from tests.BaseTestClass import BaseTestClass

# not working well in radians...
fortyFive = (2 * pi) / 8

class TestPointMethods(BaseTestClass):

    circle_8th = -1 * (2 * pi)/8


    def setUp(self):
        self.a = Point(5, 5)
        self.b = Point(-5, -5)
        self.c = Point(0, 0)

        self.xDistance = 100
        self.d = Point(self.a.x + self.xDistance, self.a.y)

        self.e = Point(3, 4)  # Carpenter check. easy distance check sqrt((3)**2 + (4)**2) == 5
        self.carpenterDist = 5

        self.f = Point(5, 0)
        self.g = Point(-5, 0)
        self.h = Point(5, -5)
        self.i = Point(-3, -4)
        self.j = Point(-5, 5)

    def test_subtract(self):
        # use distance from c to approximate equal comparison.
        dist = (self.a - self.d).distance(self.c)
        self.assertAlmostEqual(dist, self.xDistance, 4)

    def test_equal(self):
        self.assertAlmostEqual(self.a.distance(self.a), 0, 2)

    def test_gt(self):
        self.assertTrue(self.a > self.b)

    def test_gtequal(self):
        self.assertTrue(self.a >= self.a)

    def test_lt(self):
        self.assertTrue(self.b < self.a)

    def test_ltequal(self):
        self.assertTrue(self.a <= self.a)

    def test_distance(self):
        self.assertAlmostEqual(self.a.distance(self.d), self.xDistance, 4)

    def test_distance_carpenter(self):
        self.assertAlmostEqual(self.e.distance(self.c), self.carpenterDist, 4)

    def test_angleDistTo(self):
        theta, dist = self.c.angleDistTo(self.a)
        self.assertAlmostEqual(theta, fortyFive, 4)
        self.assertAlmostEqual(dist, sqrt((5**2) + (5**2)), 4)

        theta, dist = self.c.angleDistTo(self.c)
        self.assertAlmostEqual(theta, 0, 4)
        self.assertAlmostEqual(dist, 0, 4)

    def test_reduceToAngle(self):
        testAngle = (fortyFive * 10)
        expectedAngle = (fortyFive * 2)
        self.assertAlmostEqual(Point._reduceToTwoPi(testAngle), expectedAngle, 4)

        testAngle = (fortyFive * 18)
        expectedAngle = (fortyFive * 2)
        self.assertAlmostEqual(Point._reduceToTwoPi(testAngle), expectedAngle, 4)

        testAngle = (fortyFive * 5)
        expectedAngle = (fortyFive * -3)
        self.assertAlmostEqual(Point._reduceToTwoPi(testAngle), expectedAngle, 4)

        testAngle = (fortyFive * -5)
        expectedAngle = (fortyFive * 3)
        self.assertAlmostEqual(Point._reduceToTwoPi(testAngle), expectedAngle, 4)

    def test_angleThreePoint1(self):
        deltaAngle = self.c.angleThreePoint(self.a, self.f)
        self.assertAlmostEqual(-1 * fortyFive, deltaAngle, 4)

        deltaAngle = self.c.angleThreePoint(self.f, self.a)
        self.assertAlmostEqual(fortyFive, deltaAngle, 4)

        deltaAngle = self.c.angleThreePoint(self.a, self.g)
        self.assertAlmostEqual(fortyFive * 3, deltaAngle, 4)

        deltaAngle = self.c.angleThreePoint(self.g, self.a)
        self.assertAlmostEqual(-1 * fortyFive * 3, deltaAngle, 4)

        # ambiguous.  Going with what atan2 reports.
        deltaAngle = self.c.angleThreePoint(self.a, self.b)
        self.assertAlmostEqual(-1 * fortyFive * 4, deltaAngle, 4)

        deltaAngle = self.c.angleThreePoint(self.b, self.a)
        self.assertAlmostEqual(fortyFive * 4, deltaAngle, 4)

    def test_angleThreePoint2(self):
        # Angle from h to a is positive in counter clockwise direction
        deltaAngle = self.c.angleThreePoint(self.h, self.a)
        self.assertAlmostEqual(deltaAngle, fortyFive * 2, 4)

        # Angle from a to h is negative in the clockwise direction.
        deltaAngle = self.c.angleThreePoint(self.a, self.h)
        self.assertAlmostEqual(deltaAngle, fortyFive * -2, 4)

        deltaAngle = self.a.angleThreePoint(self.j, self.h)
        self.assertAlmostEqual(deltaAngle, fortyFive * 2, 4)

        deltaAngle = self.a.angleThreePoint(self.h, self.j)
        self.assertAlmostEqual(deltaAngle, fortyFive * -2, 4)

        deltaAngle = self.b.angleThreePoint(self.g, self.f)
        expectedAngle = -1 * fortyFive * 2 + atan2(5, 10)
        self.assertAlmostEqual(deltaAngle, expectedAngle, 4)

    def test_midpoint(self):
        mp = self.a.segment(self.b)[0]
        self.assertAlmostEqual(mp.x, 0, 3)
        self.assertAlmostEqual(mp.y, 0, 3)

        mp = self.b.segment(self.a)[0]
        self.assertAlmostEqual(mp.x, 0, 3)
        self.assertAlmostEqual(mp.y, 0, 3)

    def test_segment(self):
        mp = self.a.segment(self.b)
        self.assertAlmostEqual(mp[0].x, 0, 3)
        self.assertAlmostEqual(mp[0].y, 0, 3)

        mp = self.b.segment(self.a)
        self.assertAlmostEqual(mp[0].x, 0, 3)
        self.assertAlmostEqual(mp[0].y, 0, 3)

        sl = self.a.segment(self.b, 1)
        expectedPointList = [Point(2.5, 2.5), Point(0, 0), Point(-2.5, -2.5)]
        self.assertEqual(len(sl), 3)
        for ep, mp in zip(expectedPointList, sl):
            self.assertAlmostEqual(ep.x, mp.x, 4)
            self.assertAlmostEqual(ep.y, mp.y, 4)

    def test_segmentGen(self):
        mp = self.a.segmentGen(self.b)
        expectedPoint = Point(0,0)
        self.assertPoint(next(mp),expectedPoint)

        mp = self.b.segmentGen(self.a)
        self.assertPoint(next(mp),expectedPoint)

        sl = self.a.segment(self.b, 1)
        expectedPointList = [Point(2.5, 2.5), Point(0, 0), Point(-2.5, -2.5)]
        self.assertEqual(len(sl), 3)
        for ep, mp in zip(expectedPointList, sl):
            self.assertPoint(ep, mp)

if __name__ == '__main__':
    unittest.main()
