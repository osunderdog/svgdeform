import unittest
from SVGDrawArea.Point import Point
from tests.BaseTestClass import BaseTestClass


class TestStringMethods(BaseTestClass):

    def test_init(self):
        x = 10
        y = 100
        da = Point(x, y)
        self.assertEqual('Point:(10,100)', str(da))


if __name__ == '__main__':
    unittest.main()
