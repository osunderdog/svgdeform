import unittest

from SVGDrawArea.Point import Point
from SVGDrawArea.Spiral import Spiral, Sweep, Path
from tests.BaseTestClass import BaseTestClass


class MyTestCase(BaseTestClass):
    def setUp(self) -> None:
        self.sp = Spiral(anglesPerRot=90, rotations=3)

    def test_construct(self):
        print(self.sp.info())

    def test_sweep(self):
        spccw = Spiral(anglesPerRot=4, rotations=3, sweep=Sweep.COUNTERCLOCKWISE)
        spcw = Spiral(anglesPerRot=4, rotations=3, sweep=Sweep.CLOCKWISE)

        self.assertNotAlmostEqual(spccw.angleStep,spcw.angleStep, 4)
        self.assertAlmostEqual(spccw.angleStep,-1 * spcw.angleStep,4)

    def test_path(self):
        spout = Spiral(anglesPerRot=4, rotations=2.5)
        spin = Spiral(anglesPerRot=4, rotations=2.5, path=Path.IN)

        #How to test?  One should be the inverse of the other
        self.assertListEqual(list(spout.angleIncrementGenerator()),
                             list(spin.angleIncrementGenerator())[::-1])

    def test_angleIncrementGenerator(self):
        g = self.sp.angleIncrementGenerator()
        print(list(g))

    def test_spiralpointgenerator(self, centerPoint=Point(0, 0)):
        gen = self.sp.spiralpointgenerator(centerPoint)
        print(list(gen)[0:10])


if __name__ == '__main__':
    unittest.main()
