from unittest import TestCase

from SVGDrawArea.DeformRegion import bytwo
from tests.BaseTestClass import BaseTestClass


class TestBytwo(BaseTestClass):
    def test_bytwo(self):
        a = iter([1, 2, 3, 4, 5, 6])
        i = bytwo(a)
        self.assertEqual(next(i), (None, 1))
        self.assertEqual(next(i), (1, 2))
        self.assertEqual(next(i), (2, 3))
        self.assertEqual(next(i), (3, 4))
        self.assertEqual(next(i), (4, 5))
        self.assertEqual(next(i), (5, 6))
        self.assertRaises(StopIteration, next, i)
