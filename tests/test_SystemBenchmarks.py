import time
from unittest import TestCase

from SVGDrawArea.DeformRegion import DeformRegion, defaultDeformPointFill
from SVGDrawArea.Point import Point
from tests.BaseTestClass import BaseTestClass


class timed_test:
    def __init__(self, loopCount=1, expectedTime=None):
        self.loopCount = loopCount
        self.expectedTime = expectedTime

    ## https://python-3-patterns-idioms-test.readthedocs.io/en/latest/PythonDecorators.html
    ## https://stackoverflow.com/questions/24761790/timing-a-unit-test-including-the-set-up
    def __call__(self, f):
        def wrapped(testself, *args, **kwargs):
            testTimeSum = 0
            for _ in range(self.loopCount):
                start = time.time()
                f(testself, *args, **kwargs)
                end = time.time()
                testTime = end - start
                testTimeSum += testTime
            # print("testTimeList:{:6f}".format(testTimeList))
            averageTime = testTimeSum / self.loopCount
            print("Function: {} Runs:{:6d}, Average:{:6f}".format(f, self.loopCount, averageTime))
            if self.expectedTime is not None:
                testself.assertGreater(self.expectedTime, averageTime,
                                       "ExpectedTime:{} ActualTime:{}".format(self.expectedTime, averageTime))
            else:
                testself.assertTrue(True)


        return wrapped

class TestSystemBenchmark(BaseTestClass):

    # second parameter is minimum time
    @timed_test(1000000, 0.000001)
    def test_point(self):
        Point(0, 0)

    @timed_test(10000, 0.003)
    def test_defaultDeformPointFill(self):
        # Will be impacted.  Point angle is small enough
        center = Point(0, 0)
        nextPoint = Point(10, 0)
        lastPoint = Point(0, 10)

        defaultDeformPointFill(center, nextPoint, lastPoint, 0, 10)
