from itertools import repeat, chain
from math import pi

from SVGDrawArea.DeformRegion import DeformRegion, defaultDeformPointFill, defaultDeformFunction
from SVGDrawArea.Point import Point
from SVGDrawArea.Spiral import Spiral
from tests.BaseTestClass import BaseTestClass

class TestDeformRegion(BaseTestClass):
    def __init__(self, *args, **kwargs):
        super(TestDeformRegion, self).__init__(*args, *kwargs)

    def test_deformValue(self):
        result = defaultDeformFunction(angle=0, distance=0.01, radius=1)
        SmallDeform = (1 - 0.01 / 1) * 0.75 * 1 + 0.01
        self.assertAlmostEqual(result[1], SmallDeform, 6)

    def test_deform(self):
        def simpleDeform(a, d, r):
            return (a, 10)

        d = DeformRegion(center=Point(10, 10), radius=10, deformFunction=simpleDeform)
        deformedVector = d.deform(pi, 0, 5)
        self.assertAlmostEqual(deformedVector[0], pi)
        self.assertAlmostEqual(deformedVector[1], 10)

    def test_deformRegion(self):
        def simpleDeform(a, d, r):
            return (a, 10)

        d = DeformRegion(Point(10, 10), 10, simpleDeform)
        dp = d.deformRegion(Point(20, 20))
        ep = Point(20, 20)
        self.assertPoint(dp, ep)

        pointList = [Point(x, 10) for x in range(5, 18, 2)]
        deformedList = [d.deformRegion(p) for p in pointList]
        print("result: {}".format(list(deformedList)))
        pushedLeft = Point(0, 10)
        pushedRight = Point(20, 10)
        expectedList = chain(repeat(pushedLeft, 3), repeat(pushedRight, 3))

        for e, a in zip(expectedList, deformedList):
            self.assertPoint(a, e)

    def test_defaultDeformPointFill01(self):
        # should not be impacted. Point angle is too large
        center = Point(0, 0)
        nextPoint = Point(10, 0)
        lastPoint = Point(0, 10)
        pointFillList = defaultDeformPointFill(center, nextPoint, lastPoint, pi, 2)
        for p in pointFillList:
            self.assertPoint(p, nextPoint)

    def test_defaultDeformPointFill02(self):
        # Will be impacted.  Point angle is small enough
        center = Point(0, 0)
        nextPoint = Point(10, 0)
        lastPoint = Point(0, 10)

        point_fill_gen = defaultDeformPointFill(center, nextPoint, lastPoint, 0, 0)
        # expect one midpoint between last and next as well as next.
        self.assertEqual(len(list(point_fill_gen)), 2, "Should be two points")

        point_fill_gen = defaultDeformPointFill(center, nextPoint, lastPoint, 0, 0)
        # last item in pointFillList should be nextPoint.
        segmentPointList = nextPoint.segment(lastPoint, 0)
        self.assertPoint(segmentPointList[0], next(point_fill_gen), "Should be midpoint first")
        self.assertPoint(nextPoint, next(point_fill_gen), "last item in list should be nextPoint.")

        point_fill_gen = defaultDeformPointFill(center, nextPoint, lastPoint, 0, 1)
        point_fill_list = list(point_fill_gen)
        self.assertEqual(len(point_fill_list), 4, "Should be four points")
        self.assertPoint(nextPoint, point_fill_list[-1], "last item in list should be nextPoint.")

        point_fill_gen = defaultDeformPointFill(center, nextPoint, lastPoint, 0, 2)
        self.assertEqual(len(list(point_fill_gen)), 8, "Should be eight points")

    def test_deformRegion2(self):

        def simpleDeform(a, d, r):
            # passtrhough deform.  Don't do anything to angle or distance parameters.
            return (a, d)

        def simpleDeformFill(center: Point,
                             nextPoint: Point,
                             lastPoint: Point,
                             injectPointsAngle: float,
                             injectMidpointLevel: int):
            # Just inject a few points to verify that they can be injected.
            return [Point(2, 2), Point(2, 2), Point(2, 2), nextPoint]

        d = DeformRegion(center=Point(6, 6),
                         radius=4,
                         deformFunction=simpleDeform,
                         defaultPointFillFunction=simpleDeformFill,
                         injectMidpointLevel=7,
                         injectPointsAngle=0)
        lastPoint = Point(0,0)
        nextPoint = Point(6, 4)
        deformedList = d.deformRegion2(pointIter=iter([lastPoint,nextPoint]))
        expectedList = [Point(0,0),Point(2, 2), Point(2, 2), Point(2, 2), nextPoint]

        for e, a in zip(expectedList, deformedList):
            self.assertPoint(a, e)
    def test_notebook_example(self):
        cp = Point(400,400)
        sp = Spiral(anglesPerRot=90, rotations=20)
        deform1 = DeformRegion(center=Point(150, 150),
                               radius=60,
                               injectPointsAngle=(2 * pi) / 16,
                               injectMidpointLevel=7)
        dr1 = deform1.deformRegion2(sp.spiralpointgenerator(cp))
        # for p in dr1:
        #     print(p)
        # #not sure what to test here...