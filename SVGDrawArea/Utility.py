from itertools import tee
from typing import Tuple, Iterable

"""Consume the iterable.
a = [1,2,3,4,5,6]
i = bytwo(a)
next(i) => (none,1)
"""
PointPair = Tuple[int, int]
def bytwo(items: Iterable[int]) -> PointPair:
    lastValue = None
    for value in items:
        yield (lastValue, value)
        lastValue = value

## from
## https://docs.python.org/3/library/itertools.html
##
def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    for i in iterable:
        yield (i,next(iterable))
