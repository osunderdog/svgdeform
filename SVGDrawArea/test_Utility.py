from unittest import TestCase
from Utility import bytwo, pairwise

class TestUtility(TestCase):
    def test_bytwo(self):
        input = iter(['a','b','c','d','e'])
        expected = [(None,'a'),('a','b'),('b','c'),('c','d'),('d','e')]
        self.assertSequenceEqual(list(bytwo(input)),expected)

    def test_pairwise(self):
        "s -> (s0,s1), (s1,s2), (s2, s3), ..."
        input = iter(['a','b','c','d','e','f'])
        expected = [('a','b'),('c','d'),('e','f')]
        self.assertSequenceEqual(list(pairwise(input)), expected)
