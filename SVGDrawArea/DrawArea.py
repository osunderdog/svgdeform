import drawSvg as Draw

from SVGDrawArea.Point import Point
from SVGDrawArea.Utility import pairwise

class DrawArea:
    def __init__(self, dx=None, dy=None):
        self.__drawing = None
        self.__dx = dx
        self.__dy = dy

    def __str__(self) -> str:
        return "DrawArea: dx:{},dy:{}".format(self.__dx, self.__dy)

    def grid_background(self, hlinestep: int, vlinestep: int, centerpoint=0, **kwargs )->None:
        self.hlines(spacing=hlinestep, **kwargs)
        self.vlines(spacing=vlinestep, **kwargs)
        if centerpoint > 0:
            self.append(Draw.Circle(*self.center, centerpoint))

    def print_cross(self, center:Point, xdist:float, ydist:float, **kwargs):
        self.get_drawing().append(Draw.Line(center.x-xdist,center.y,
                  center.x+xdist, center.y,
                  **kwargs))
        self.get_drawing().append(Draw.Line(center.x,center.y-ydist,
                  center.x, center.y+ydist,
                  **kwargs))

    def print_background(self, hborderoffset=10, vborderoffset=10, xdist=10,ydist=10, **kwargs):
        """create edge markers but no paths on drawing area."""
        cornerPoints = [Point(self.min_x + hborderoffset, self.min_y + vborderoffset), ##lower left
                        Point(self.max_x - hborderoffset, self.min_y + vborderoffset), ##lower right
                        Point(self.max_x - hborderoffset, self.max_y - vborderoffset), ##upper right
                        Point(self.min_x + hborderoffset, self.max_y - vborderoffset), ##upper left
                        ]
        for p in cornerPoints:
            self.print_cross(p,xdist=xdist, ydist=ydist, **kwargs)

    def get_drawing(self):
        if self.__drawing is None:
            self.__drawing = self.gen_drawing()
        return self.__drawing

    def gen_drawing(self):
        return Draw.Drawing(self.__dx, self.__dy,
                            center=(0, 0))

    def append(self, obj):
        self.get_drawing().append(obj)

    @property
    def center(self):
        return Point(self.max_x/2,self.max_y/2)

    @property
    def min_y(self):
        return 0

    @property
    def max_y(self):
        return self.__dy

    @property
    def min_x(self):
        return 0

    @property
    def max_x(self):
        return self.__dx

    def hlines(self, spacing, **kwargs):
        # given area, draw hlines
        self.get_drawing().extend(Draw.Line(self.min_x, y, self.max_x, y, **kwargs)
                                for y in range(self.min_y, self.max_y, spacing))

    def vlines(self, spacing, **kwargs):
        self.get_drawing().extend(Draw.Line(x, self.min_y, x, self.max_y, **kwargs)
                                for x in range(self.min_x, self.max_x, spacing))

    def points(self, plist, **kwargs):
        self.get_drawing().extend(Draw.Circle(*p, **kwargs) for p in plist)

    def drawLinePath(self, ptiter, **args):
        linePath = Draw.Path(**args)
        linePath.M(*(next(ptiter)))
        for pt in ptiter:
            linePath.L(*pt)
        self.append(linePath)

    def drawSmoothPath(self, ptiter, **args):
        linePath = Draw.Path(**args)
        linePath.M(*(next(ptiter)))

        ##by two lastPoint is None.
        for lastPoint, nextPoint in pairwise(ptiter):
            linePath.Q(*lastPoint, *nextPoint)
        self.append(linePath)
