from math import pi
from typing import Iterable, Tuple, List

from SVGDrawArea.Point import Point
from SVGDrawArea.Utility import bytwo

ARPair = Tuple[float, float]

def defaultDeformFunction(angle: float, distance: float, radius: float) -> ARPair:
    return angle, ((1 - distance / radius) * 0.75 * radius) + distance


def defaultDeformPointFill(center: Point,
                           nextPoint: Point,
                           lastPoint: Point,
                           injectPointsAngle: float,
                           injectMidpointLevel: int) -> List[Point]:
    """if the angle between last point and next point relative to the
    deformCenter is greater than ?? then fill
    in additional points by extrapolating a line...
    center: the point around which the deformation takes place.
    nextPoint: The next point that will be deformed
    lastPoint: The previous point that was deformed
    deformCenter: The center point of the deformation operation
    injectPointsAngle: if the angle between last point and next point is greater than injectPointsAngle,
    then inject some points that will be deformed before next point in order to smooth the curve
    injectMidpointLevel: how many segment midpoints to inject when injectPointsAngle exceeded.
    """
    if abs(center.angleThreePoint(lastPoint, nextPoint)) > injectPointsAngle:
        for p in lastPoint.segment(nextPoint, injectMidpointLevel):
            yield p
    yield nextPoint


class DeformRegion:

    def __init__(self, center: float, radius: float,
                 deformFunction=defaultDeformFunction,
                 injectPointsAngle=(2 * pi) / 16,
                 injectMidpointLevel=7,
                 defaultPointFillFunction=defaultDeformPointFill) -> None:
        super().__init__()
        self.center = center
        self.radius = radius
        self.deformFunction = deformFunction
        self.injectPointsAngle = injectPointsAngle
        self.injectMidpointLevel = injectMidpointLevel
        self.deformPointFillFunction = defaultPointFillFunction

    def deform(self, angle: float, distance: float, radius: float) -> ARPair:
        """Actually perform the deformation -- return a new angle and radius.
        Overload this function if you want a different deformation.

        :param angle: angle from deform center to point that will be deformed
        :param distance: distance from deform center to point that will be deformed
        :return: tuple containing new angle and new distance for point that is being deformed.
        """
        return self.deformFunction(angle, distance, radius)

    def deformRegion(self, point: Point):
        if self.center.angleDistTo(point)[1] > self.radius:
            # point outside range of consideration for deform
            return point
        else:
            # translate point to it's new home
            # 1) calculate angle and distance between point and deformCenter.
            # 2) Run angle and distance through deformFunction to derive new angle and distance
            # 3) Translate angle and distance back to a point
            # 4) Move point back relative to deformCenter
            return self.center.fromAngle(*self.deform(*(self.center.angleDistTo(point)), self.radius))


    def deformRegion2(self, pointIter) -> Point:
        # todo yield
        for lastPoint, point in bytwo(pointIter):
            if lastPoint is None:
                # if the first point is part of the deform then don't do anything
                yield point
            elif self.center.angleDistTo(point)[1] > self.radius:
                # point outside range of consideration for deform
                yield point
            else:
                # point is inside range for deform
                for pt in self.deformPointFillFunction(center=self.center,
                                                       nextPoint=point,
                                                       lastPoint=lastPoint,
                                                       injectPointsAngle=self.injectPointsAngle,
                                                       injectMidpointLevel=self.injectMidpointLevel):
                    yield self.center.fromAngle(*self.deform(*self.center.angleDistTo(pt), self.radius))

