from math import pi, cos, sin, atan2, hypot, floor
from typing import TypeVar, List

T = TypeVar('T', bound='Point')

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def tuple(self) -> [float, float]:
        return self.x, self.y

    def __abs__(self) -> T:
        return (Point(abs(self.x), abs(self.y)))

    def __add__(self, other: T) -> T:
        return (Point(self.x + other.x, self.y + other.y))

    def __sub__(self, other: T) -> T:
        return (Point(self.x - other.x, self.y - other.y))

    def __eq__(self, other: T) -> bool:
        return (self.x == other.x and self.y == other.y)

    def __ne__(self, other: T) -> bool:
        return (self.x != other.x or self.y != other.y)

    def distance(self, p: T) -> float:
        # pythgorean
        tp = p - self
        return hypot(tp.x,tp.y)

    def __ge__(self, other: T) -> bool:
        return (self.x >= other.x) and (self.y >= other.y)

    def __gt__(self, other: T) -> bool:
        return (self.x > other.x) and (self.y > other.y)

    def __lt__(self, other: T) -> bool:
        return (self.x < other.x) and (self.y < other.y)

    def __le__(self, other: T) -> bool:
        return (self.x <= other.x) and (self.y <= other.y)

    def __str__(self) -> str:
        return "Point:({},{})".format(self.x,self.y)

    def __repr__(self) -> str:
        return "Point:({0:0.3f},{1:0.3f})".format(*self)

    def __iter__(self) -> iter:
        return iter((self.x, self.y))

    def __getitem__(self, i:int) -> float:
        return ((self.x, self.y)[i])

    def __len__(self):
        return 2

    def __truediv__(self, v):
        if isinstance(v, int):
            return Point(self.x / v, self.y / v)
        elif isinstance(v, Point):
            return Point(self.x / v.x, self.y / v.y)

    @classmethod
    def fromAngleAbs(cls, angle: float, radius: float) -> T:
        """convert an angle in radians and radius to Point"""
        angle = cls._reduceToTwoPi(angle=angle)
        return cls(radius * cos(angle), radius * sin(angle))

    @staticmethod
    def _reduceToTwoPi(angle: float) -> float:
        if angle >= 2 * pi:
            # reduce down to something between 0 and 2 * pi
            angle -= floor(angle / (2*pi)) * (2*pi)
        if angle <= -2 * pi:
            angle += floor(angle / (2 * pi)) * (2 * pi)
        if angle > pi:
            angle = -1 * ((2 * pi) - angle)
        if angle < -pi:
            angle = ((2 * pi) + angle)
        return angle

    def fromAngle(self, angle: float, radius: float) -> T:
        """Calculate new Point using origin point and an angle, and radius."""
        return Point.fromAngleAbs(angle, radius) + self

    def angleDistTo(self, other: T) -> [float, float]:
        '''Calculate the angle and distance from self to other.  Return as tuple (angle,dist)'''
        p = other - self
        angle = atan2(p.y, p.x)
        return angle, self.distance(other)

    def angleThreePoint(self, a: T, b: T) -> float:
        """using self as center point calculate the delta between angle from self to a and self to b"""
        aAngle, _ = self.angleDistTo(a)
        bAngle, _ = self.angleDistTo(b)
        return Point._reduceToTwoPi(bAngle - aAngle)

    # TODO resolve how a recursive generator would work...
    def segmentGen(self, a: T, recurseLevel=0):
        for i in self.segment(a, recurseLevel):
            yield i

    ##Recursive Generator?
    def segment(self, a: T, recurseLevel=0) -> List[T]:
        """recursively generate midpoints for some iteration.
        Points are ordered from self to a"""
        resultList = []
        mp = Point((self.x + a.x) / 2, (self.y + a.y) / 2)
        if recurseLevel == 0:
            return [mp]
        if recurseLevel > 0:
            resultList.extend(self.segment(mp, recurseLevel - 1))
            resultList.append(mp)
            resultList.extend(mp.segment(a, recurseLevel - 1))
        return resultList
