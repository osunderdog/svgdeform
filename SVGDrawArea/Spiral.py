from math import pi, ceil
from enum import Enum, unique


from SVGDrawArea.Point import Point

TWOPI = 2 * pi

@unique
class Sweep(Enum):
    '''
    COUNTERCLOCKWISE: Sweep in positive radians from 0 through PI then -PI through to 0
    CLOCKWISE: Sweep through negative radians from 0 through -PI then PI to 0
    '''
    COUNTERCLOCKWISE = 1
    CLOCKWISE = 2

@unique
class Path(Enum):
    '''
    OUT: plot points starting from center and working outward
    IN: plot points starting from edge and working inward.
    '''
    OUT = 1
    IN = 2

class Spiral:

    def linearRadiusGrowthFunc(i: int, a: float) -> float:
        return i * 0.11

    def __init__(self,
                 anglesPerRot,
                 rotations,
                 sweep=Sweep.COUNTERCLOCKWISE,
                 path=Path.OUT,
                 startAngle=0,
                 radiusGrowthFunc=linearRadiusGrowthFunc):
        self.rotations = rotations
        self.anglesPerRot = anglesPerRot

        self.sweep = sweep

        ##TODO Something is messed up with CLOCKWISE/COUNTERCLOCKWISE
        ##Think about the relationship between sweep and path.
        ##path = out sweep = counterclockwise
        ##path = in sweep = clockwise
        self.angleStep = TWOPI / anglesPerRot if sweep == Sweep.COUNTERCLOCKWISE else -1 * TWOPI / anglesPerRot

        #total Steps is an integer.
        self.totalSteps = ceil(self.anglesPerRot * self.rotations) if self.anglesPerRot > 0 else floor(self.anglesPerRot * self.rotations)

        self.path = path
        self.startAngle = startAngle

        self.totalAngle = self.totalSteps * self.angleStep
        self.radiusGrowthFunc = radiusGrowthFunc
        self.radius = self.radiusGrowthFunc(self.totalSteps, self.totalAngle)
        self.radiusIncreasePerRotation = self.radius / self.rotations
        self.radiusIncreasePerAngle = self.radiusIncreasePerRotation / self.anglesPerRot

    def info(self):
        print('              Sweep: {0:}'.format(self.sweep))
        print('               Path: {0:}'.format(self.path))
        print('        Start angle: {0:.3f}'.format(self.startAngle))
        print('          Rotations: {0:.3f}'.format(self.rotations))
        print('Angles Per Rotation: {0:.3f}'.format(self.anglesPerRot))
        print('         Angle Step: {0:.3f}'.format(self.angleStep))
        print('        Total Steps: {0:.3f}'.format(self.totalSteps))
        print('        Total Angle: {0:.3f}'.format(self.totalAngle))
        print('             Radius: {0:.3f}'.format(self.radius))
        print('   Radius + Per Rot: {0:.3f}'.format(self.radiusIncreasePerRotation))
        print('   Radius + Per Ang: {0:.3f}'.format(self.radiusIncreasePerAngle))

    def angleIncrementGenerator(self):
        #nuance here because range finishes before end value
        #glad I caught this with testing!

        rangeStep = range(0, self.totalSteps + 1, 1) \
            if self.path == Path.OUT else range(self.totalSteps, -1, -1)

        startAngle = self.startAngle
        return ((i, startAngle + (self.angleStep * i)) for i in rangeStep)

    def spiralpointgenerator(self, centerPoint=Point(0, 0)):
        angleIncrementGenerator = self.angleIncrementGenerator()

        return map(lambda ia: centerPoint.fromAngle(angle=ia[1],
                                                    radius=self.radiusGrowthFunc(ia[0], ia[1])),
                   angleIncrementGenerator)
