
def applyWithIndex(iterable, func):
    '''
    Apply function to values in iterable. function should accept value and index of value.
    '''
    return [func(value, index) for index,value in enumerate(iterable)]

def translateFromOriginTo(iterable, cp):
    return map(lambda pt: pt + cp, iterable)
