#!/usr/bin/env python3

import sys

#sys.path.append('/home/brucelowther/src/python/svgnotebook/myprojectenv/lib/python3.5/site-packages')

import drawSvg as Draw
from SVGDrawArea.DrawArea import DrawArea
from SVGDrawArea.Point import Point
import argparse

filename = None

##Expected input
# bwl_simple_program.py: error: unrecognized arguments:
# --radio1=string3 --radio2=test3! /tmp/ink_ext_XXXXXX.svgL3374Z

def parseArguments():
    parser = argparse.ArgumentParser(description='go once or many')
    parser.add_argument("--radio1", help='something about radio1')
    parser.add_argument("--radio2", help='something about radio2')
    parser.add_argument("filename", nargs=1, help='svg file to process')
    
    return(parser.parse_args())

def fileorstdout(target):
    try:
        return open(target, 'w')
    except TypeError:
        return sys.stdout

def main():
    args = parseArguments()

    da = DrawArea(450,250)
    da.hlines(spacing=25, opacity=0.1)
    da.vlines(spacing=25, opacity=0.1)
    cp = Point(20,20)
    da.append(Draw.Circle(*cp.tuple(),20))

    p_start = Point(25,75)
    p_control_a = Point(50,150)
    p_control_b = Point(75,75)
    p_assumed = Point(p_control_b.y, p_control_b.x)
    p_end = Point(150,150)
    da.points((p_start,p_control_a, p_control_b, p_end, p_assumed), radius=3.0, fill="green")


    p = Draw.Path(fill="#ffffff", fill_opacity=0.1, stroke="black")

    p.M(*(p_start.tuple()))
    p.Q(*(p_control_a.tuple()),
        *(p_control_b.tuple()))
    p.T(*(p_end.tuple()))
    da.append(p)

    da.get_drawing().asSvg(fileorstdout(filename))


if __name__ == "__main__":
    main()




