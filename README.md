#  Execution Notes

To startup a notebook to view SVG results as they are created
```
source myprojectenv/bin/activate.fish 
jupyter notebook
```

IF you need to set the password for the notebook

```bash
jupyter notebook password
```

Installing packages 'for development'  so that they can be altered..
This approach links the development file 
```
python3 setup.py develop
```



This is so that I can continue development on the package and see
results without having to re-install 'pip install' every time I change
some minute detail.



