
##Deploy the inkscape extension python script
deploy:
	scp inkscape/MQTBez.py	$(HOME)/.config/inkscape/extensions/MQTBez.py

##Run tests
test:
	python -m unittest discover -s tests -v

##install package in root area so that it'll get picked up by inkscape.
install:
	sudo /usr/bin/python3 setup.py install

uninstall:
	sudo pip3 uninstall SVGDrawArea

